﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvcModelbindingTestGeometrie.Models
{
    public class Kreis
    {
        public double Durchmesser { get; set; }
        public double Radius { get; set; }
        public double Umfang { get; set; }
        public double Fläche { get; set; }
        public bool IsRadius { get; set; }

        public Kreis()
        { }

        public Kreis(double wert, bool isRadius)
        {
            IsRadius = isRadius;
            if (isRadius == true)
            {
                Radius = wert;
            }
            else
            {
                Durchmesser = wert;
            } 
        }

        public void RadiusBerechnen()
        {
            Umfang = 2 * Math.PI * Radius;
            Fläche = Math.PI * Math.Pow(Radius, 2);
            Durchmesser = Radius * 2;
        }

        public void DurchmesserBerechnen()
        {
            Umfang = Math.PI * Durchmesser;
            Fläche = Math.PI * (Math.Pow(Durchmesser, 2) / 4);
            Radius = Durchmesser / 2;
        }
        
    }
}