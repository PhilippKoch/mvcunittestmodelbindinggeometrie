﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcModelbindingTestGeometrie.Models
{
    public class KreisBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Kreis k = null;

            bool test = bindingContext.ModelType.Equals(typeof(Kreis));

            if (bindingContext.ModelType.Equals(typeof(Kreis)))
            {
                k = new Kreis();

                if (GetValue<bool>(bindingContext, "radius") == true)
                {
                    k.Radius = GetValue<double>(bindingContext, "wert");
                    k.RadiusBerechnen();
                }
                else
                {
                    k.Durchmesser = GetValue<double>(bindingContext, "wert");
                    k.DurchmesserBerechnen();
                }

                k.IsRadius = GetValue<bool>(bindingContext, "radius");
            }

            return k;
        }

        private T GetValue<T>(ModelBindingContext bindingContext, string key)
        {
            try
            {
                ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(key);
                bindingContext.ModelState.SetModelValue(key, valueResult);
                return (T)valueResult.ConvertTo(typeof(T));
            }
            catch
            {
                return default(T);
            }
            
        }
    }
}