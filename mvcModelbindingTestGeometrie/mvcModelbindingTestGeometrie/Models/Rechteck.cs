﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvcModelbindingTestGeometrie.Models
{
    public class Rechteck
    {
        public double Höhe { get; set; }
        public double Breite { get; set; }
        public double Umfang { get; set; }
        public double Fläche { get; set; }

        public Rechteck(double höhe, double breite)
        {
            Höhe = höhe;
            Breite = breite;
        }

        public Rechteck()
        { }

        public void Berechnen()
        {
            Umfang = 2 * (Höhe + Breite);
            Fläche = Höhe * Breite;
        }


    }
}