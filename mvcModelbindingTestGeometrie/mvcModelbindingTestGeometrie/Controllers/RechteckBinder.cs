﻿using mvcModelbindingTestGeometrie.Models;
using System;
using System.Web.Mvc;

namespace mvcModelbindingTestGeometrie.Controllers
{
    public class RechteckBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Rechteck r = null;

            if (bindingContext.ModelType.Equals(typeof(Rechteck)))
            {
                r = new Rechteck();

                r.Breite = GetValue<double>(bindingContext, "breite");
                r.Höhe = GetValue<double>(bindingContext, "höhe");
                r.Berechnen();
            }

            return r;
        }

        private T GetValue<T>(ModelBindingContext bindingContext, string key)
        {
            try
            {
                ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(key);
                bindingContext.ModelState.SetModelValue(key, valueResult);
                return (T)valueResult.ConvertTo(typeof(T));
            }
            catch
            {
                return default(T);
            }

        }
    }
}