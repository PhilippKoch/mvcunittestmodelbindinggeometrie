﻿using mvcModelbindingTestGeometrie.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcModelbindingTestGeometrie.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View("Index");
        }

        //[ModelBinder(typeof(KreisBinder))]Kreis derKreis
        //string wert, bool radius
        public ActionResult KreisBerechnen([ModelBinder(typeof(KreisBinder))]Kreis derKreis)
        {
            //Kreis derKreis = new Kreis(double.Parse(wert), radius);

            ViewBag.Radius = derKreis.Radius;
            ViewBag.Durchmesser = derKreis.Durchmesser;
            ViewBag.KreisU = derKreis.Umfang;
            ViewBag.KreisF = derKreis.Fläche;
            return View("Index");
        }

        public ActionResult RechteckFormular()
        {
            return View("RechteckFormular");
        }

        public ActionResult RechteckBerechnen([ModelBinder(typeof(RechteckBinder))]Rechteck dasRechteck)
        {
            ViewBag.Höhe = dasRechteck.Höhe;
            ViewBag.Breite = dasRechteck.Breite;
            ViewBag.RechteckU = dasRechteck.Umfang;
            ViewBag.RechteckF = dasRechteck.Fläche;
            return View("RechteckFormular");
        }
    }
}