﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mvcModelbindingTestGeometrie.Models;
using System.Web.Mvc;
using mvcModelbindingTestGeometrie.Controllers;

namespace mvcModelbindingTestGeometrie.Tests
{
    [TestClass]
    public class UnitTest1
    {
        #region Klassenmethoden
        [TestMethod]
        public void KreisUmfangDurchmesser()
        {
            double erwarteterUmfang = Math.PI;
            Kreis k = new Kreis(1,false);
            k.DurchmesserBerechnen();

            Assert.AreEqual(erwarteterUmfang, k.Umfang, 0.001);

        }

        [TestMethod]
        public void KreisUmfangRadius()
        {
            double erwarteterUmfang = Math.PI;
            Kreis k = new Kreis(0.5, true);
            k.RadiusBerechnen();

            Assert.AreEqual(erwarteterUmfang, k.Umfang, 0.001);
        }

        [TestMethod]
        public void KreisFlächeDurchmesser()
        {
            double erwarteteFläche = 0.7853;
            Kreis k = new Kreis(1,false);
            k.DurchmesserBerechnen();

            Assert.AreEqual(erwarteteFläche, k.Fläche, 0.001);
        }

        [TestMethod]
        public void KreisFlächeRadius()
        {
            double erwarteteFläche = 0.7853;
            Kreis k = new Kreis(0.5,true);
            k.RadiusBerechnen();

            Assert.AreEqual(erwarteteFläche, k.Fläche, 0.001);
        }


        [TestMethod]
        public void RechteckUmfang()
        {
            double erwarteterUmfang = 4;
            Rechteck r = new Rechteck(1,1);
            r.Berechnen();

            Assert.AreEqual(erwarteterUmfang, r.Umfang);
        }

        [TestMethod]
        public void RechteckFläche()
        {
            double erwarteteFläche = 1;
            Rechteck r = new Rechteck(1, 1);
            r.Berechnen();

            Assert.AreEqual(erwarteteFläche, r.Fläche);
        }
        #endregion

        #region Views

        [TestMethod]
        public void IndexTest()
        {
            const string expectedViewName = "Index";
            var controller = new HomeController();

            var result = controller.Index() as ViewResult;

            Assert.IsNotNull(result,"Sollte ein ViewResult zurückliefer");

            Assert.AreEqual(expectedViewName, result.ViewName,
                string.Format("Name der View sollte {0} sein", expectedViewName));
        }

        [TestMethod]
        public void KreisTest()
        {
            const string expectedViewName = "Index";
            var controller = new HomeController();

            var result = controller.KreisBerechnen(new Kreis(1,false)) as ViewResult;

            Assert.IsNotNull(result, "Sollte ein ViewResult zurückliefer");

            Assert.AreEqual(expectedViewName, result.ViewName,
                string.Format("Name der View sollte {0} sein", expectedViewName));
        }

        [TestMethod]
        public void RechteckTest()
        {
            const string expectedViewName = "RechteckFormular";
            var controller = new HomeController();

            var result = controller.RechteckBerechnen(new Rechteck(1, 1)) as ViewResult;

            Assert.IsNotNull(result, "Sollte ein ViewResult zurückliefer");

            Assert.AreEqual(expectedViewName, result.ViewName,
                string.Format("Name der View sollte {0} sein", expectedViewName));
        }

        [TestMethod]
        public void RechteckFormularTest()
        {
            const string expectedViewName = "RechteckFormular";
            var controller = new HomeController();

            var result = controller.RechteckFormular() as ViewResult;

            Assert.IsNotNull(result, "Sollte ein ViewResult zurückliefer");

            Assert.AreEqual(expectedViewName, result.ViewName,
                string.Format("Name der View sollte {0} sein", expectedViewName));
        }


        #endregion
    }
}
